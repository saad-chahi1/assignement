package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;

import java.util.ArrayList;
import java.util.List;

public class CompteMapper {

    public static CompteDto mapToDto(Compte compte) {
        if(compte == null){
            return null;
        }
        CompteDto compteDto = CompteDto.builder()
                .nrCompte(compte.getNrCompte())
                .rib(compte.getRib())
                .solde(compte.getSolde())
                .utilisateur(compte.getUtilisateur())
                .build();
        return compteDto;
    }

    public static Compte mapFromDto(CompteDto compteDto) {
        if(compteDto == null){
            return null;
        }
        Compte compte = new Compte();
        compte.setNrCompte(compteDto.getNrCompte());
        compte.setRib(compteDto.getRib());
        compte.setSolde(compteDto.getSolde());
        compte.setUtilisateur(compteDto.getUtilisateur());

        return compte;
    }

    public static List<CompteDto> mapToListDto(List<Compte> comptes){
        List<CompteDto> compteDtos = new ArrayList<>();
        for(Compte compte : comptes){
            compteDtos.add(CompteMapper.mapToDto(compte));
        }
        return compteDtos;
    }
}
