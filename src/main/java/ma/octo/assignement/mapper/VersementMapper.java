package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VersementMapper {
    public static VersementDto mapToDto(Versement versement) {

        VersementDto versementDto = VersementDto.builder()
                .motifVersement(versement.getMotifVersement())
                .nomEmetteur(versement.getNomEmetteur())
                .rib(Objects.requireNonNull(versement.getCompteBeneficiaire()).getRib())
                .dateExecution(versement.getDateExecution())
                .montantVersement(versement.getMontantVersement())
                .build();

        return versementDto;

    }
    public static Versement mapFromDto(VersementDto versementDto) {

        Compte compteBeneficiaire = new Compte();
        compteBeneficiaire.setRib(versementDto.getRib());

        Versement versement = new Versement();

        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setCompteBeneficiaire(compteBeneficiaire);
        versement.setDateExecution(versementDto.getDateExecution());
        versement.setNomEmetteur(versementDto.getNomEmetteur());

        return versement;

    }

    public static List<VersementDto> mapToListDto(List<Versement> versements){
        List<VersementDto> versementDtos = new ArrayList<>();
        for(Versement versement : versements){
            versementDtos.add(VersementMapper.mapToDto(versement));
        }
        return versementDtos;
    }
}
