package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.IVirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class VirementServiceImp implements IVirementService {

    @Autowired
    VirementRepository virementRepository;

    @Override
    public List<VirementDto> getVirements() {
        List<Virement> virements = virementRepository.findAll();
        VirementMapper virementMapper = new VirementMapper();
        List<VirementDto> virementDtos = new ArrayList<>();
        for(Virement virement : virements){
            virementDtos.add(virementMapper.map(virement));
        }
        return virementDtos;
    }
}
