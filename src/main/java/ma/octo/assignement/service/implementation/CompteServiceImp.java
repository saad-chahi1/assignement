package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompteServiceImp implements ICompteService {

    Logger LOGGER = LoggerFactory.getLogger(CompteServiceImp.class);

    @Autowired
    private CompteRepository compteRepository;


    @Override
    public List<CompteDto> listComptes() {
        List<Compte> comptes = compteRepository.findAll();
        return CompteMapper.mapToListDto(comptes);
    }

    @Override
    public CompteDto getCompteByNr(String numCompte) {
        Compte compte = compteRepository.findByNrCompte(numCompte);
        return CompteMapper.mapToDto(compte);
    }

    @Override
    public CompteDto updateCompte(CompteDto compteDto) throws Exception {
        Compte compte = compteRepository.findByNrCompte(compteDto.getNrCompte());
        if(compte == null) throw new Exception("compte n'existe pas");
        compte.setSolde(compte.getSolde());

        return CompteMapper.mapToDto(compteRepository.save(compte));
    }
}
