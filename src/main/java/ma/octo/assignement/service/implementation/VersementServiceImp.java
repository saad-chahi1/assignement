package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.IVersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VersementServiceImp implements IVersementService {

    private final VersementRepository versementRepository;
    private final IAuditService auditService;
    private final CompteRepository compteRepository;

    Logger LOGGER = LoggerFactory.getLogger(VersementServiceImp.class);

    public VersementServiceImp(VersementRepository versementRepository, IAuditService auditService, CompteRepository compteRepository) {
        this.versementRepository = versementRepository;
        this.auditService = auditService;
        this.compteRepository = compteRepository;
    }

    @Override
    public VersementDto getVersement(Long id) throws Exception {
        Versement versement = versementRepository.findById(id).orElseThrow(()->{
            LOGGER.error("Versement " + id  +" non existant");
            return new CompteNonExistantException("Versement non existant");
        });
        return VersementMapper.mapToDto(versement);
    }

    @Override
    public List<VersementDto> getVersements() {
        List<Versement> versements = versementRepository.findAll();
        return VersementMapper.mapToListDto(versements);
    }

    @Override
    public VersementDto verser(VersementDto versementDto) throws Exception {

        Compte compteBeneficaire = compteRepository.findCompteByRib(versementDto.getRib());


        if (compteBeneficaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        verifyVersement(versementDto);

        compteBeneficaire.setSolde(new BigDecimal(compteBeneficaire.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compteRepository.save(compteBeneficaire);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDateExecution());
        versement.setCompteBeneficiaire(compteBeneficaire);
        versement.setNomEmetteur(versementDto.getNomEmetteur());
        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setMotifVersement(versementDto.getMotifVersement());

        Versement addedVersement = versementRepository.save(versement);

        auditService.auditVersement("Versement vers " + addedVersement
                .getCompteBeneficiaire().getNrCompte() + " d'un montant de " + addedVersement.getMontantVersement()
                .toString());
        return VersementMapper.mapToDto(addedVersement);
    }

    @Override
    public void deleteVersement(Long id) throws Exception {

        if (versementRepository.findById(id).isEmpty()) {
            LOGGER.error("Versement Non existant");
            throw new TransactionException("Versement Non existant");
        }
        versementRepository.deleteById(id);
        LOGGER.error("Versement de id = "+id+" est supprimé");
    }

    @Override
    public VersementDto updateVersement(Long id, VersementDto versementDto) throws Exception {

        Compte compteBeneficaire = compteRepository.findCompteByRib(versementDto.getRib());

        if (compteBeneficaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        verifyVersement(versementDto);

        Versement versement = versementRepository.findById(id).orElseThrow(() -> {
            System.out.println("Versement Non existant");
            return new TransactionException("Versement Non existant");
        });

        int soldeActuel = compteBeneficaire.getSolde().intValue() + versementDto.getMontantVersement().intValue() - versement.getMontantVersement().intValue();

        compteBeneficaire.setSolde(new BigDecimal(soldeActuel));
        compteRepository.save(compteBeneficaire);

        versement.setDateExecution(versementDto.getDateExecution());
        versement.setCompteBeneficiaire(compteBeneficaire);
        versement.setNomEmetteur(versementDto.getNomEmetteur());
        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setMontantVersement(versementDto.getMontantVersement());

        versementRepository.save(versement);

        auditService.auditVersement("Update Versement vers " + versementDto
                .getRib() + " d'un montant de " + versementDto.getMontantVersement()
                .toString());

        return VersementMapper.mapToDto(versement);
    }

    private void verifyVersement(VersementDto versementDto) throws TransactionException {
        if (versementDto.getMontantVersement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        }

        if (versementDto.getMotifVersement().isBlank()) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }
    }
}
