package ma.octo.assignement.service;

import ma.octo.assignement.dto.VirementDto;

import java.util.List;

public interface IVirementService {
    List<VirementDto> getVirements();
}
