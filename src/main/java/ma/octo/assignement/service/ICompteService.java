package ma.octo.assignement.service;

import ma.octo.assignement.dto.CompteDto;

import java.util.List;

public interface ICompteService {
    List<CompteDto> listComptes();
    CompteDto getCompteByNr(String numCompte);
    CompteDto updateCompte(CompteDto compteDto) throws Exception;
}
