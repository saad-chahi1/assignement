package ma.octo.assignement.service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IVersementService {
    List<VersementDto> getVersements();
    VersementDto verser(VersementDto versementDto) throws Exception;
    void deleteVersement(Long id) throws Exception;
    VersementDto updateVersement(Long id, VersementDto versementDto) throws Exception;
    VersementDto getVersement(Long id) throws Exception;
}
