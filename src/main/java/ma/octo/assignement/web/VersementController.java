package ma.octo.assignement.web;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.implementation.VersementServiceImp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/versements")
public class VersementController {

    Logger LOGGER = LoggerFactory.getLogger(VersementController.class);

    @Autowired
    private VersementServiceImp versementService;

    @GetMapping
    List<VersementDto> AllVersements() {
        List<VersementDto> all = versementService.getVersements();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }
    @GetMapping("/{id}")
    public VersementDto getVersementById(@PathVariable Long id) throws Exception {
        return versementService.getVersement(id);
    }
    @PostMapping
    public VersementDto Verser(@RequestBody VersementDto versementDto) throws Exception {
        return versementService.verser(versementDto);
    }
    @PutMapping("/{id}")
    public VersementDto updateVersement(@RequestBody VersementDto versementDto, @PathVariable Long id) throws Exception {
        return versementService.updateVersement(id, versementDto);
    }
    @DeleteMapping("/{id}")
    public void deleteVersement(@PathVariable Long id) throws Exception {
        versementService.deleteVersement(id);
    }
}
