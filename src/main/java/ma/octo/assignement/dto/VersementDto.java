package ma.octo.assignement.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
public class VersementDto {
    private Long id;

    private BigDecimal montantVersement;

    private Date dateExecution;

    private String nomEmetteur;

    private String rib;

    private String motifVersement;
}
