package ma.octo.assignement.dto;

import lombok.Builder;
import lombok.Data;
import ma.octo.assignement.domain.Utilisateur;
import java.math.BigDecimal;

@Data
@Builder
public class CompteDto {

    private Long id;

    private String nrCompte;

    private String rib;

    private BigDecimal solde;

    private Utilisateur utilisateur;
}
