package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.repository.VersementRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class VersementServiceImpTest {

    @Mock
    private VersementRepository versementRepository;

    @InjectMocks
    private VersementServiceImp versementServiceImp;

    @Test
    void getVersement() {
    }

    @Test
    void getVersements() {
        Versement versement1 = new Versement();
        versement1.setMotifVersement("paiement");
        versement1.setMontantVersement(new BigDecimal(100));
        versement1.setCompteBeneficiaire(new Compte());
        versement1.setNomEmetteur("saad");

        Versement versement2 = new Versement();
        versement2.setMotifVersement("paiement 2");
        versement2.setMontantVersement(new BigDecimal(130));
        versement2.setCompteBeneficiaire(new Compte());
        versement2.setNomEmetteur("chahi");

        List<Versement> versementList = Arrays.asList(versement1, versement2);


        Mockito.when(versementRepository.findAll()).thenReturn(versementList);
        List<VersementDto> versementDtoList = versementServiceImp.getVersements();

        assertEquals("paiement", versementDtoList.get(0).getMotifVersement());
        assertEquals("saad", versementDtoList.get(0).getNomEmetteur());
        assertEquals(new BigDecimal(100), versementDtoList.get(0).getMontantVersement());

        assertEquals("paiement 2", versementDtoList.get(1).getMotifVersement());
        assertEquals("chahi", versementDtoList.get(1).getNomEmetteur());
        assertEquals(new BigDecimal(130), versementDtoList.get(1).getMontantVersement());
    }

    @Test
    void verser() {
    }

    @Test
    void deleteVersement() {
    }

    @Test
    void updateVersement() {
    }
}