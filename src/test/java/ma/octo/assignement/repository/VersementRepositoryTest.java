package ma.octo.assignement.repository;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VersementRepositoryTest {

  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext wac;

  @Before("")
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

  }
  @Test
  public void verifyAllVersementList() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/versements").accept(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$", hasSize(4))).andDo(print());
  }

  @Test
  public void verifyInvalidVersementId() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/versements/0").accept(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.errorCode").value(404))
            .andExpect(jsonPath("$.message").value("Versement non existant"))
            .andDo(print());
  }
}